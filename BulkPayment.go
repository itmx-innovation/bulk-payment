package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

type BulkPaymentChaincode struct {
}

type CreditTransferTransaction struct {
	BatchNumber string `json:"batchNumber`

	ReceivingBankCode   string `json:"receivingBankCode"`
	ReceivingBranchCode string `json:"receivingBranchCode"`
	ReceivingBankAccNum string `json:"receivingBankAccNum"`
	ReceiverInfo        string `json:"receiverInfo"`

	SendingBankCode   string `json:"sendingBankCode"`
	SendingBranchCode string `json:"sendingBranchCode"`
	SendingBankAccNum string `json:"sendingBankAccNum"`
	SenderInfo        string `json:"senderInfo"`

	EffectiveTransferDate string `json:"effectiveTransferDate"`
	ServiceTypeCode       string `json:"serviceTypeCode"`
	TransferAmount        string `json:"transferAmount"`

	OtherInfo               string `json:"otherInfo"`
	OriginalReferenceNumber string `json:"orgRefNumber"`

	Status string `json:"status"`
}

type TransactionProposedEvent struct {
	SenderBank     string `json:"senderBank"`
	ReceiverBank   string `json:"receiverBank"`
	TransactionKey string `json:"transactionKey"`
	Transaction	   CreditTransferTransaction `json:"transaction"`
	RequestedTimestamp string `json:"requestedTimestamp"`
}

type TransactionProcessStatusEvent struct {
	SenderBank     string `json:"senderBank"`
	ReceiverBank   string `json:"receiverBank"`
	TransactionKey string `json:"transactionKey"`
	Status         string `json:"status"`
}

func (t *BulkPaymentChaincode) Init(stub shim.ChaincodeStubInterface) peer.Response {
	return shim.Success(nil)
}

func (t *BulkPaymentChaincode) Query(stub shim.ChaincodeStubInterface) peer.Response {
	return shim.Success(nil)
}

func (t *BulkPaymentChaincode) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	if function == "createTransferTransaction" {
		return t.createTransferTransaction(stub, args)
	} else if function == "updateReceivingProcessStatus" {
		return t.updateReceivingProcessStatus(stub, args)
	} else if function == "queryByDate" {
		return queryByDate(stub, args)
	} else if function == "getTransaction" {
		return t.getTransaction(stub, args)
	} else {
		return shim.Error("Function is not supported")
	}
}

/**
 * Create transfer transaction for Sending bank
 * The transaction's status is always "PROPOSED" and needed to be signed by
 * Sender Bank, Receiver Bank and ITMX
 **/
func (t *BulkPaymentChaincode) createTransferTransaction(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	var transaction CreditTransferTransaction

	if len(args) != 16 {
		return shim.Error("Invalid arguments")
	}

	key := args[0]

	transaction.BatchNumber = args[1]

	// Receiver
	transaction.ReceivingBankCode = args[2]
	transaction.ReceivingBranchCode = args[3]
	transaction.ReceivingBankAccNum = args[4]
	transaction.ReceiverInfo = args[5]

	// Sender
	transaction.SendingBankCode = args[6]
	transaction.SendingBranchCode = args[7]
	transaction.SendingBankAccNum = args[8]
	transaction.SenderInfo = args[9]

	// Payment info
	transaction.EffectiveTransferDate = args[10]
	transaction.ServiceTypeCode = args[11]
	transaction.TransferAmount = args[12]
	transaction.OtherInfo = args[13]
	transaction.OriginalReferenceNumber = args[14]

	// Status
	transaction.Status = "PROPOSED"

	requested := args[15]

	// Serialized transaction data
	b, err := json.Marshal(transaction)
	if err != nil {
		return shim.Error("Unable to serialized transaction data")
	}

	// Create transaction in blockchain
	err = stub.PutState(key, b)
	if err != nil {
		return shim.Error("Unable to commit transaction")
	} else {
		// Publish event
		var event = TransactionProposedEvent{transaction.SendingBankCode, transaction.ReceivingBankCode, key, transaction, requested}
		eventBytes, err := json.Marshal(event)
		if err != nil {
			fmt.Println("Could not serialized event") // Need better solution
		}
		err = stub.SetEvent("bulkTransactionProposedEvent", eventBytes)
		if err != nil {
			fmt.Println("Could not send custom event")
		}
	}

	return shim.Success(nil)
}

/**
*	Function for receiver bank to reply to original message when process the transaction
* that proposed by sender bank
* Parameters:
* 	- key
*		- status (bool): true(PROCESS_SUCCEED), false(PROCESS_FAILED)
 */
func (t *BulkPaymentChaincode) updateReceivingProcessStatus(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	if len(args) != 2 {
		return shim.Error("Invalid arguments")
	}

	key := args[0]
	status, err := strconv.ParseBool(args[1])
	if err != nil {
		return shim.Error("Invalid status. expecting 'true' or 'false'")
	}

	var statusTxt string
	if status == true {
		statusTxt = "PROCESS_SUCCEED"
	} else {
		statusTxt = "PROCESS_FAILED"
	}

	// Read transaction data from blockchain
	var txAsBytes []byte
	txAsBytes, err = stub.GetState(key)
	if err != nil {
		return shim.Error("Unable to find data")
	}

	if txAsBytes == nil {
		return shim.Error("Unable to read data from blockchain")
	}

	// Convert raw byte data to struct
	var transaction CreditTransferTransaction
	err = json.Unmarshal(txAsBytes, &transaction)
	if err != nil {
		return shim.Error("Unable to deserialize data, data might be corrupted")
	}

	// Update transaction Status
	transaction.Status = statusTxt

	// Serialized data for update blockchain
	var b []byte
	b, err = json.Marshal(transaction)
	if err != nil {
		return shim.Error("Unable to serialized data")
	}

	// Write updated data to blockchain
	err = stub.PutState(key, b)
	if err != nil {
		return shim.Error("Unable to update blockchain data")
	} else {
		// publish event
		var event = TransactionProcessStatusEvent{transaction.SendingBankCode, transaction.ReceivingBankCode, key, statusTxt}
		eventBytes, err := json.Marshal(event)
		if err != nil {
			fmt.Println("Cannot serialized event")
		}
		err = stub.SetEvent("bulkStatusEvent", eventBytes)
		if err != nil {
			fmt.Println("Cannot publish custom event")
		}
	}

	// Success
	return shim.Success(nil)
}

func (t *BulkPaymentChaincode) getTransaction(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	if len(args) != 1 {
		return shim.Error("Invalid argument")
	}

	//var tx CreditTransferTransaction
	valueAsBytes, err := stub.GetState(args[0])
	if err != nil {
		return shim.Error(err.Error())
	}

	if valueAsBytes == nil {
		return shim.Error("Cannot get value for given key")
	}

	/**
	err = json.Unmarshal(valueAsBytes, &tx)
	if err != nil {
		return shim.Error(err.Error())
	}

	transactionAsBytes, _ := json.Marshal(tx)
	return shim.Success(transactionAsBytes)
	**/
	return shim.Success(valueAsBytes)
}

func queryByDate(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	if len(args) != 2 {
		return shim.Error("Invalid arguments")
	}

	startKey := args[0]
	endKey := args[1]

	var buffer bytes.Buffer
	buffer.WriteString("[")
	bArrayMemberAlreadyWritten := false

	// -- Get all transaction in range ---//
	resultIterator, err := stub.GetStateByRange(startKey, endKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultIterator.Close()

	for resultIterator.HasNext() {
		aKeyValue, err := resultIterator.Next()
		if err != nil {
			shim.Error(err.Error())
		}

		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}

		buffer.WriteString("{\"Key\":")
		buffer.WriteString("\"")
		buffer.WriteString(aKeyValue.Key)
		buffer.WriteString("\"")

		buffer.WriteString(", \"Record\":")
		buffer.WriteString(string(aKeyValue.Value))
		buffer.WriteString("}")

		bArrayMemberAlreadyWritten = true
	}

	buffer.WriteString("]")
	return shim.Success(buffer.Bytes())
}

func main() {
	err := shim.Start(new(BulkPaymentChaincode))
	if err != nil {
		fmt.Println("Unable to start Credit transfer chaincode")
	} else {
		fmt.Println("Credit transfer chaincode is successfully started")
	}
}
